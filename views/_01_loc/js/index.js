
var msg='hello';

function fun(x){
    y=(x||0);
    console.info(x);

}

let add = function (x,y) {
    return x+y;
};
var fun3=() =>console.log('箭头函数');
//注意箭头函数没有自己的this,arguments,super或new.target
fun(10);
fun3();
var ret2 = fun('张三');
console.log(ret2);

console.log(add(3, 4));
//普通函数printAddResult,参数why,参数可以是一个函数,函数why(?,?)带了两个参数
function printAddResult(why) {
    console.log(why(3,9));
}
//调用普通方法printAddResult,并打匿名函数add传递进去,匿名函数add获得参数,并按照匿名函数add的方法体调用
printAddResult(add);

var fun2=(username,age)=>{
    console.log(username);
    console.log(age);
};
fun2("小明",18);
//调用普通方法并传入参数,此参数可以使用箭头函数,这里箭头函数作为匿名函数使用
printAddResult((x,y)=>{
    return x*y;

},6);
function f() {
    console.log(arguments);
}
f(1,2,3);
f();
f('张三','什么?',18);
