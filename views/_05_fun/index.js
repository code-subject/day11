//普通函数定义
function doWork1(username){
    console.log(username);
    return username;
}

function doWork2(username){
    console.log("无返回的函数");

}

//匿名函数
let add=function (a,b) {
    return a+b;
}

//箭头函数
let fun1=()=>{
    console.log("箭头函数:无参数无返回");
}
//箭头函数,只有一个参数不需要圆括号,只有一个方法体不需要花括号
//无参数需要圆括号
let fun2=username=> console.log("箭头函数:无参数无返回");

console.log(doWork1('张三'));;
console.log(doWork2('李四'));;
console.log(add(9, 1));;
console.log(fun1());;
console.log(fun2('什么?'));

//构造函数(类)的定义
function Person(name,age) {
    this.name=name;
    this.age=age;
    this.sleep=function () {
        console.log("困了,睡一觉")
    }
}
var person = new Person("赵云",28);
console.log(person);
console.log(person.name);
person.sleep();
person.name="刘备";
console.log(person.name);
person.why='我不理解..0.0.';
console.log(person);
//构造函数也称之为类

//数组的创建有三种方式
var arr=new Array(4);
var arr=new Array("张三",'李四','王五');
console.log(arr);
var arr=[1,2,3,4];
console.log(arr);
//数组的遍历方式
for (let i = 0; i <arr.length ; i++) {
    console.log(arr[i]);
}
arr.forEach(function (item,index,array) {
   // console.log(arguments);
    console.log(item,index,array);
})